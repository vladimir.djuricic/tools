#!/bin/bash
CURRENT_DIR=`pwd`
REPOSITORY=$1
REPOSITORY_BRANCH=$2
REMOTE_PATH=$3
LOCAL_PATH=$4
TMP_DIR=$(mktemp -d -t ci-XXXXXXXXXX)
cd $TMP_DIR 
git clone -b $REPOSITORY_BRANCH -n  $REPOSITORY --depth 1 . 
git checkout HEAD $REMOTE_PATH
if [[ -z $LOCAL_PATH ]]; then
    mv $REMOTE_PATH $CURRENT_DIR
else
    if [[ "$LOCAL_PATH" = /* ]]; then
        mv $REMOTE_PATH $LOCAL_PATH
    else
        mv $REMOTE_PATH $CURRENT_DIR/$LOCAL_PATH
    fi 
fi

cd $CURRENT_DIR && rm -rf $TMP_DIR
